from environs import Env


env = Env()

BIND_HOST = env('BIND_HOST', default='127.0.0.1')
BIND_PORT = env.int('BIND_PORT', default=8080)

NEO4J_URI = env('NEO4J_URI', default="bolt://host.docker.internal:7687")
NEO4J_AUTH = env('NEO4J_AUTH', default='neo4j')
NEO4J_PASSWORD = env('NEO4J_PASSWORD', default='test')
