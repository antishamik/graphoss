FROM python:3.9
ENV PYTHONUNBUFFERED=1
ENV PYTHONPATH="."
RUN apt update -y && apt upgrade -y

ENV GRAPHOSS_ROOT /app
RUN mkdir -p $GRAPHOSS_ROOT

WORKDIR $GRAPHOSS_ROOT


COPY ../../../requirements/production.txt requirements.txt
COPY ../../../commands/ commands/
RUN pip install -r requirements.txt

COPY ../.. .

COPY server.py /
COPY gunicorn.conf /
RUN pip install gunicorn
RUN pip install -r requirements.txt


CMD ["gunicorn", "-c", "gunicorn.conf", "server:app"]
