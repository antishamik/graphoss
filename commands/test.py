import logging

from commands.clean_up import delete_node_command
from commands.create_relation import create_relation_command
from src.test.graph_test import test_get_node, test_get_nodes, test_create_node, test_update_node, test_create_relation, \
    test_update_relation, test_delete_node, test_delete_relation, test_shortest_path
from create_node import create_node_command

delete_node_command()
create_node_command()
create_relation_command()


def get_node():
    try:
        test_get_node()
    except AssertionError as e:
        logging.exception(msg="TEST_GET_NODE ERROR: {}".format(e))
    logging.warning('TEST_GET_NODE: PASSED')


def get_nodes():
    try:
        test_get_nodes()
    except AssertionError as e:
        logging.exception(msg="TEST_GET_NODES ERROR: {}".format(e))
    logging.warning('TEST_GET_NODES: PASSED')


def create_node():
    try:
        test_create_node()
    except AssertionError as e:
        logging.exception(msg="TEST_CREATE_NODE ERROR: {}".format(e))
    logging.warning('TEST_CREATE_NODE: PASSED')


def update_node():
    try:
        test_update_node()
    except AssertionError as e:
        logging.exception(msg="TEST_UPDATE_NODE ERROR: {}".format(e))
    logging.warning('TEST_UPDATE_NODE: PASSED')


def create_relation():
    try:
        test_create_relation()
    except AssertionError as e:
        logging.exception(msg="TEST_CREATE_RELATION ERROR: {}".format(e))
    logging.warning('TEST_CREATE_RELATION: PASSED')


def update_relation():
    try:
        test_update_relation()
    except AssertionError as e:
        logging.exception(msg="TEST_UPDATE_RELATION ERROR: {}".format(e))
    logging.warning('TEST_UPDATE_RELATION: PASSED')


def shortest_path():
    try:
        test_shortest_path()
    except AssertionError as e:
        logging.exception(msg="TEST_SHORTEST_PATH ERROR: {}".format(e))
    logging.warning('TEST_SHORTEST_PATH: PASSED')


def delete_relation():
    try:
        test_delete_relation()
    except AssertionError as e:
        logging.exception(msg="TEST_DELETE_RELATION ERROR: {}".format(e))
    logging.warning('TEST_DELETE_RELATION: PASSED')


def delete_node():
    try:
        test_delete_node()
    except AssertionError as e:
        logging.exception(msg="TEST_DELETE_NODE ERROR: {}".format(e))
    logging.warning('TEST_DELETE_NODE: PASSED')


get_node()
get_nodes()
create_node()
update_node()
create_relation()
update_relation()
shortest_path()
delete_relation()
delete_node()
delete_node_command()
