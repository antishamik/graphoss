from src.service.services import CreateServices

create_service = CreateServices()


def create_relation_command():
    nodes = [{'node1': 'test_1',
              'node2': 'test_2',
              'cost': 30},
             {'node1': 'test_5',
              'node2': 'test_9',
              'cost': 20},
             {'node1': 'test_5',
              'node2': 'test_6',
              'cost':   15},
             {'node1': 'test_1',
              'node2': 'test_7',
              'cost': 22},
             {'node1': 'test_7',
              'node2': 'test_2',
              'cost': 19},
             {'node1': 'test_9',
              'node2': 'test_5',
              'cost': 12},
             {'node1': 'test_2',
              'node2': 'test_9',
              'cost': 45},
             {'node1': 'test_10',
              'node2': 'test_2',
              'cost': 11}
             ]

    for value in nodes:
        create_service.create_relation(node1=value['node1'], node2=value['node2'], cost=value['cost'])


create_relation_command()
