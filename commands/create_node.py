from src.service.services import CreateServices

create_service = CreateServices()


def create_node_command():
    values = ['test_1',
              'test_2',
              'test_3',
              'test_4',
              'test_5',
              'test_6',
              'test_7',
              'test_8',
              'test_9',
              'test_10']
    for value in values:
        create_service.create_node(name=value)


create_node_command()
