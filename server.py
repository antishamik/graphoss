from aiohttp import web

import config
from app import create_app

app = create_app()

if __name__ == '__main__':
    web.run_app(app, host=config.BIND_HOST, port=config.BIND_PORT)
