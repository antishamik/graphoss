class ReadQueries:
    list_node_query = '''
        MATCH (n:Node) RETURN ID(n), n.name LIMIT $limit
    '''
    get_node_query = '''
        MATCH (n:Node {name: $name}) RETURN ID(n), n.name
    '''

    get_shortest_path = ''' 
    MATCH (n1:Node { name: $node1 })
    MATCH (n2:Node { name: $node2 })
    CALL apoc.algo.dijkstra(n1, n2, 'ARROW>', 'cost') YIELD path, weight
    RETURN path, weight
    '''


class CreateQueries:
    create_node_query = '''
        MERGE (n:Node {name: $name})
    '''
    create_relation_query = '''
        MATCH (node1:Node {name: $node1})
        MATCH (node2:Node {name: $node2})
        CREATE (node1)-[r:ARROW {cost: $cost}]->(node2)
    '''



class UpdateQueries:
    update_node_query = '''
       MATCH (n:Node {name: $name})
       SET n.name = $new_name
    '''

    update_relation_query = '''
    MATCH (node1:Node {name: $node1})-[rel:ARROW]-(node2:Node {name: $node2})
    SET rel.cost = {cost: $cost}
    RETURN rel
    '''


class DeleteQueries:
    delete_node_query = '''
       MATCH (n:Node {name: $name})
       DETACH DELETE n
    '''
    delete_relation_query = '''
    MATCH (:Node{name: $node1})-[r:ARROW]->(:Node{name: $node2})
    DELETE r
    '''
