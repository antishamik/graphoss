import functools
import json

from graphene.test import Client

from app import schema


def test_get_node():
    client = Client(schema=schema)
    executed = client.execute('''{node(name: "test_2"){name}}''')
    assert executed == {
        "data": {
            "node": {
                "name": "test_2",
            }
        }
    }


def test_get_nodes():
    client = Client(schema=schema)
    executed = client.execute('''{nodes(limit: 10){name}}''')
    control_data = [
        {
            "name": "test_1",

        },
        {
            "name": "test_2",

        },
        {
            "name": "test_3",

        },
        {
            "name": "test_4",

        },
        {
            "name": "test_5",

        },
        {
            "name": "test_6",

        },
        {
            "name": "test_7",

        },
        {
            "name": "test_8",

        },
        {
            "name": "test_9",

        },
        {
            "name": "test_10",
        }
    ]
    if functools.reduce(lambda x, y: x and y, map(lambda p, q: p == q, control_data, executed['data']['nodes']), True):
        assert True


def test_shortest_path():
    client = Client(schema=schema)
    executed = client.execute(''' {shortestPath(node1:"test_1", node2:"test_9"){path,weight}} ''')
    assert executed['data']['shortestPath'][0]['path'] == ["{'name': 'test_1'}",
                                                           'ARROW',
                                                           "{'name': 'test_2'}",
                                                           'ARROW',
                                                           "{'name': 'test_9'}"]

    assert executed['data']['shortestPath'][0]['weight'] == 75


def test_create_node():
    client = Client(schema=schema)
    executed = client.execute('''mutation{createNode(name:"test_115"){ok}}''')
    assert executed == {
        'data': {
            "createNode": {
                "ok": True
            }
        }
    }


def test_update_node():
    client = Client(schema=schema)
    executed = client.execute('''mutation{updateNode(name:"test_115", newName:"test_11"){ok}}''')
    assert executed == {
        'data': {
            "updateNode": {
                "ok": True
            }
        }
    }


def test_create_relation():
    client = Client(schema=schema)
    executed = client.execute('''mutation{createRelation(node1:"test_11", node2:"test_1", cost:30){
        ok
    }}''')
    assert executed == {
        'data': {
            "createRelation": {
                "ok": True
            }
        }
    }


def test_update_relation():
    client = Client(schema=schema)
    executed = client.execute('''mutation{updateRelation(node1:"test_11", node2:"test_1", cost:20){
    ok}
    }''')
    assert executed == {
        'data': {
            "updateRelation": {
                "ok": True
            }
        }
    }


def test_delete_node():
    client = Client(schema=schema)
    executed = client.execute('''mutation{deleteNode(name:"test_115"){ok}}''')
    assert executed == {
        'data': {
            "deleteNode": {
                "ok": True
            }
        }
    }


def test_delete_relation():
    client = Client(schema=schema)
    executed = client.execute('''mutation{deleteRelation(node1:"test_2", node2:"test_9"){ok}}''')
    assert executed == {
        'data': {
            "deleteRelation": {
                "ok": True
            }
        }
    }
