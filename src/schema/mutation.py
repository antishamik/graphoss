from graphene import ObjectType, String, Boolean, Field, Mutation, Int

from src.schema.types import Node
from src.service.services import CreateServices, UpdateServices, DeleteServices

create_service = CreateServices()
update_service = UpdateServices()
delete_service = DeleteServices()


class CreateNode(Mutation):
    class Arguments:
        name = String()

    ok = Boolean()
    node = Field(lambda: Node)

    def mutate(self, info, name):
        create_service.create_node(name)
        ok = True
        return CreateNode(ok=ok)


class CreateRelation(Mutation):
    class Arguments:
        node1 = String()
        node2 = String()
        cost = Int()

    ok = Boolean()

    def mutate(self, info, node1, node2, cost):
        create_service.create_relation(node1, node2, cost)
        ok = True
        return CreateNode(ok=ok)


class UpdateNode(Mutation):
    class Arguments:
        name = String()
        new_name = String()

    ok = Boolean()

    def mutate(self, info, name, new_name):
        update_service.update_node(name, new_name)
        ok = True
        return CreateNode(ok=ok)


class UpdateRelation(Mutation):
    class Arguments:
        node1 = String()
        node2 = String()
        cost = Int()

    ok = Boolean()

    def mutate(self, info, node1, node2, cost):
        update_service.update_relation(node1, node2, cost)
        ok = True
        return CreateNode(ok=ok)


class DeleteNode(Mutation):
    class Arguments:
        name = String()

    ok = Boolean()

    def mutate(self, info, name):
        delete_service.delete_node(name)
        ok = True
        return CreateNode(ok=ok)


class DeleteRelation(Mutation):
    class Arguments:
        node1 = String()
        node2 = String()

    ok = Boolean()

    def mutate(self, info, node1, node2):
        delete_service.delete_relation(node1, node2)
        ok = True
        return CreateNode(ok=ok)


class Mutation(ObjectType):
    create_node = CreateNode.Field()
    create_relation = CreateRelation.Field()
    update_node = UpdateNode.Field()
    delete_node = DeleteNode.Field()
    update_relation = UpdateRelation.Field()
    delete_relation = DeleteRelation.Field()
