from graphene import ObjectType, List, Int, Field, String

from src.schema.types import Node, Path
from src.service.services import ReadServices

read_service = ReadServices()


class Query(ObjectType):
    nodes = List(Node, limit=Int())
    node = Field(Node, name=String())
    shortest_path = List(Path, node1=String(), node2=String())

    def resolve_nodes(self, info, limit):
        result_nodes = read_service.list_nodes(limit)
        return [Node(id=record.get('ID(n)', None),
                     name=record.get('n.name', None)) for record in result_nodes]

    def resolve_node(self, info, name):
        record = read_service.get_node(name)
        return Node(id=record.get('ID(n)', None), name=record.get('n.name', None))

    def resolve_shortest_path(self, info, node1, node2):
        result = read_service.get_shortest_path(node1, node2)
        return [Path(path=record.get('path', None),
                     weight=record.get('weight', None)) for record in result]
