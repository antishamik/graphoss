from graphene import ObjectType, String, Int, List, NonNull


class Node(ObjectType):
    id = Int()
    name = String()


class Path(ObjectType):
    path = List(NonNull(String))
    weight = Int()
