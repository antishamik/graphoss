from neo4j import GraphDatabase

import config
from src.data.query import ReadQueries, CreateQueries, UpdateQueries, DeleteQueries


class DBService:
    driver = GraphDatabase.driver(config.NEO4J_URI, auth=(config.NEO4J_AUTH, config.NEO4J_PASSWORD))


class CreateServices(DBService):
    create_queries = CreateQueries()

    def create_node(self, name):
        with self.driver.session() as session:
            session.run(self.create_queries.create_node_query, parameters={'name': name})

    def create_relation(self, node1, node2, cost):
        with self.driver.session() as session:
            session.run(self.create_queries.create_relation_query, parameters={'node1': node1,
                                                                               'node2': node2,
                                                                               'cost': cost})


class UpdateServices(DBService):
    update_queries = UpdateQueries()

    def update_node(self, name, new_name):
        with self.driver.session() as session:
            session.run(self.update_queries.update_node_query, parameters={'name': name, 'new_name': new_name})

    def update_relation(self, node1, node2, cost):
        with self.driver.session() as session:
            session.run(self.update_queries.update_relation_query, parameters={'node1': node1,
                                                                               'node2': node2,
                                                                               'cost': cost})


class DeleteServices(DBService):
    delete_queries = DeleteQueries()

    def delete_node(self, name):
        with self.driver.session() as session:
            session.run(self.delete_queries.delete_node_query, parameters={'name': name})

    def delete_relation(self, node1, node2):
        with self.driver.session() as session:
            session.run(self.delete_queries.delete_relation_query, parameters={'node1': node1,
                                                                               'node2': node2})


class ReadServices(DBService):
    read_queries = ReadQueries()

    def list_nodes(self, limit=50):
        with self.driver.session() as session:
            result = session.run(self.read_queries.list_node_query, parameters={'limit': limit})
            return [record.data() for record in result]

    def get_node(self, name):
        with self.driver.session() as session:
            result = session.run(self.read_queries.get_node_query, parameters={'name': name})
            return result.single().data()

    def get_shortest_path(self, node1, node2):
        with self.driver.session() as session:
            result = session.run(self.read_queries.get_shortest_path, parameters={'node1': node1,
                                                                                  'node2': node2})
            return [record.data() for record in result]
