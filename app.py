from aiohttp import web
from aiohttp_graphql import GraphQLView
from graphene import Schema
from src.schema.mutation import Mutation
from src.schema.query import Query

schema = Schema(query=Query, mutation=Mutation)


def create_app() -> web.Application:
    app = web.Application()
    GraphQLView.attach(app, schema=schema, graphiql=True, route_path="/")
    return app
