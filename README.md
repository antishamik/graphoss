#### PROJECT STRUCTURE

##### Stack

1. Python 3.9
2. Neo4j Graph Database
3. Graphql

##### Source Code
Source Code has 4 layers.
1. data : Manage query and db operations
2. service: Manage logic and operations
3. schema : Python Graphane 
4. test : Manage test methods



#### PROJECT SETUP

`docker-compose up --build`

#### CLIENT
`http://127.0.0.1:8080/`

#### TEST REQUESTS 

`mutation{createNode(name:"A"),{
  ok
}}`

`mutation{createNode(name:"B"),{
  ok
}}`

`mutation{createNode(name:"C"),{
  ok
}}`

`mutation{createNode(name:"D"),{
  ok
}}`

`mutation{createNode(name:"E"),{
  ok
}}`

`mutation{createNode(name:"F"),{
  ok
}}`

`mutation{createNode(name:"G"),{
  ok
}}`

`mutation{createNode(name:"H"),{
  ok
}}`

`{
  nodes(limit:10){
    id,
    name
  }
}`

`mutation{
  createRelation(node1:"A", node2:"D", cost:20){
    ok
  }
}`

`mutation{
  createRelation(node1:"E", node2:"C", cost:40){
    ok
  }
}`

`mutation{
  createRelation(node1:"C", node2:"B", cost:15){
    ok
  }
}`

`mutation{
  createRelation(node1:"C", node2:"E", cost:11){
    ok
  }
}`

`mutation{
  createRelation(node1:"F", node2:"G", cost:5){
    ok
  }
}`

`mutation{
  createRelation(node1:"G", node2:"H", cost:43){
    ok
  }
}`

`mutation{
  createRelation(node1:"H", node2:"A", cost:47){
    ok
  }
}`

`
mutation{
  createRelation(node1:"H", node2:"C", cost:16){
    ok
  }
}
`

`
mutation{
  createRelation(node1:"B", node2:"F", cost:16){
    ok
  }
}
`

`
{
  shortestPath(node1:"H", node2:"B"){
  path,
  weight
  }
}
`





